﻿using System.Diagnostics;
using Xamarin.Forms;

namespace HelloWorld
{
    public partial class HelloWorldPage : ContentPage
    {
        public HelloWorldPage()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(HelloWorldPage)}:  constructor");
            InitializeComponent();
        }
    }
}
