# Homework 1, 2018.01.29:  10/10 #

Nice job! 

### Kudos ###

* Well done using a StackLayout to organize multiple elements on a page!
* Nice to see the debug line in the page constructor.

### Opportunities for improvement ###

* n/a